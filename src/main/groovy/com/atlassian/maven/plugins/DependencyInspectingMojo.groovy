/*
 * Copyright © 2015 Atlassian Pty Ltd. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.atlassian.maven.plugins

import org.apache.maven.artifact.repository.ArtifactRepository
import org.apache.maven.artifact.resolver.ArtifactResolver
import org.apache.maven.project.MavenProject
import org.apache.maven.project.MavenProjectHelper
import org.codehaus.gmaven.mojo.GroovyMojo

/**
 * @requiresDependencyResolution compile
 */
abstract class DependencyInspectingMojo extends GroovyMojo {
    /**
     * The scope to consider when calculating the set of dependencies to archive.
     *
     * @parameter default-value="runtime"
     */
    String includeScope

    /**
     * Artifacts which are not included in the archive or list.
     *
     * @parameter default-value=""
     */
    String excludeArtifactIds;

    /**
     * The local Maven repository.
     *
     * @parameter expression="${localRepository}"
     * @required
     * @readonly
     */
    ArtifactRepository localRepository;

    /**
     * The remote Maven repositories.
     *
     * @parameter default-value="${project.remoteArtifactRepositories}"
     * @required
     * @readonly
     */
    List<ArtifactRepository> remoteRepositories;

    /**
     * @component
     */
    ArtifactResolver artifactResolver;

    /**
     * @parameter expression="${project}"
     * @required
     * @readonly
     */
    MavenProject project

    /**
     * The Maven project helper.
     *
     * @component
     */
    MavenProjectHelper projectHelper

    /**
     * Creates the archive containing the artifacts of this project's dependencies.
     * Call from superclass and put your code in closure.
     */
    void resolveAllArtifacts(Closure processArtifacts) {
        def exclusions = excludeArtifactIds ? excludeArtifactIds.tokenize(',') : []
        exclusions.each { log.debug "Excluding artifacts with artifact id $it" }

        def allArtifacts = project.dependencyArtifacts.findAll {
            (it.scope == includeScope) && !exclusions.contains(it.artifactId)
        }
        log.debug "Considering all dependencies with '${includeScope}' scope"

        // make sure the artifacts are resolved
        allArtifacts.each { artifact ->
            if (!artifact.isResolved()) {
                artifactResolver.resolve(artifact, remoteRepositories, localRepository)
            }
        }

        processArtifacts(allArtifacts)
    }
}
