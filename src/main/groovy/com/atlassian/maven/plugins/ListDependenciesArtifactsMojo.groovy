/*
 * Copyright © 2015 Atlassian Pty Ltd. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.atlassian.maven.plugins

import org.apache.maven.plugin.MojoExecutionException
import org.apache.maven.plugin.MojoFailureException

/**
 * List dependencies as gavpc MOJO.
 *
 * @goal generate-artifact-list
 */
class ListDependenciesArtifactsMojo extends DependencyInspectingMojo {
    /**
     * The name of the output file that this MOJO will create.
     *
     * @parameter default-value='${project.build.finalName}.artifacts.txt'
     */
    String listFileName

    /**
     * Whether or not to attach the output file to the reactor's artifacts
     * and install/deploy.
     *
     * @parameter default-value=false
     */
    String attach

    @Override
    void execute() throws MojoExecutionException, MojoFailureException {
        def artifactUtil = new ArtifactUtil(null)
        resolveAllArtifacts { allArtifacts ->
            File listFile = new File("${project.build.directory}/${listFileName}")
            listFile.parentFile.mkdirs()
            listFile.withWriter { writer ->
                allArtifacts.each { artifact ->
                    writer.writeLine(artifactUtil.toString(artifact))
                }
            }

            if (attach && listFile.isFile()) {
                projectHelper.attachArtifact(project, "txt", null, listFile)
            }
        }
    }
}
