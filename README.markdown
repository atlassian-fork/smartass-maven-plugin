Smart Assembly Maven Plugin
===========================

The [Smart Assembly Maven Plugin][1] allows you to perform [Maven Assembly
Plugin][2]-like tasks in incremental fashion.

Goals
-----

### smartass:archive-dependencies

The `archive-dependencies` goal creates a ZIP archive containing all of the
project's dependencies. Whenever possible this goal updates the existing ZIP
file, as opposed to creating a new one (all comparisons are performed using the
"last modified" time stamp).  If none of the dependencies have changed, the ZIP
file is not modified.

Usage
-----

To use the plugin, configure an execution of this plugin in your `pom.xml` (see
[Configuring Build Plugins][3]). Example:

    <plugin>
      <groupId>com.atlassian.maven.plugins</groupId>
      <artifactId>smartass-maven-plugin</artifactId>
      <version>1.0</version>
      <executions>
        <execution>
          <id>create-bundled-plugins-zip</id>
          <phase>package</phase>
          <goals>
            <goal>archive-dependencies</goal>
          </goals>
          <configuration>
            <--! include artifacts with this scope -->
            <includeScope>runtime</includeScope>
          </configuration>
        </execution>
      </executions>
    </plugin>

[1]: https://bitbucket.org/atlassian/smartass-maven-plugin
[2]: http://maven.apache.org/plugins/maven-assembly-plugin/
[3]: http://maven.apache.org/guides/mini/guide-configuring-plugins.html#Configuring_Build_Plugins

